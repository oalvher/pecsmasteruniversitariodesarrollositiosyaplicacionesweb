/* Función que devuelve la pareja de adn asociada a una determinada letra */
function getCouple(character) {
  switch (character) {
    case "A":
      return [character, "T"];
    case "T":
      return [character, "A"];
    case "C":
      return [character, "G"];
    case "G":
      return [character, "C"];
  }
}

/* Función que recorre cada letra, y en caso de que la letra este permitida ya que se encuentra dentro del vector
de letras permitidas pasa a la fase de añadirse al vector junto a su pareja. En caso de que la letra no está incluida,
se devuelve al usuario que hay letras que no están permitidas. Además, la cadena que se obtiene, se pasa a mayúsculas para
no discriminar cadenas escritas con caracteres en minúsculas, y también cualquier carácter que no sea una letra se transforma
en una cadena vacía para no tener que estar pidiendo de nuevo la cadena por alguna equivocación como un espacio o caracter extraño.*/

function baseADN(string) {
  let adn = [];
  let elementsAdn = ["A", "T", "G", "C"];
  let arrayString = string.replace(/\s*/, "").toUpperCase().split("");
  for (let character of arrayString) {
    if (elementsAdn.includes(character)) {
      adn.push(getCouple(character));
    } else {
      return [];
    }
  }
  return adn;
}

/*
const string = "GCGA";
const string = "AGCA";
const string = "acGC";
const string = "a  c  G  C";
const string = "a  c  G  .C";
*/
const string = "a  c  G  .C";
if (typeof string === "string") {
  console.log(`La cadena introducida es ${string}`);
  const result = baseADN(string);
  if (result.length === 0) {
    console.log("Hay caracteres no permitidos");
  } else {
    console.log(result);
  }
} else {
  console.log("Debe introducir una cadena.");
}
