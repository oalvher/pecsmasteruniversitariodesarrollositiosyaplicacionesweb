const puntoPartida = 0; // Posición del primer punto
const x = 0; // Punto X
const y = 1; // Punto Y

/* Función que se encarga de parsear los puntos de la entrada.
Se encarga de eliminar los paréntesis, los espacios de inicio y fin y separar los dos números*/
function parsePoints(array) {
  let arrayPuntos = [];
  array.map(
    point => {
      point.replace("(", "")
        .replace(")", "")
        .trim()
        .split(/\s+/g) // Separa por 1 o más espacios que encuentre
        .reduce((pointX, pointY) => arrayPuntos.push([pointX, pointY]));
    }
  );
  return arrayPuntos;
}

/* Función que calcula todas las distancias del punto 1 al resto de puntos con el
fin de obtener la diagonal, la base y la altura */
function calculateDistance(array) {
  let arrayDistances = [];
  for (let index = 1; index < array.length; index++) {
    arrayDistances.push(Math.sqrt(Math.pow(array[puntoPartida][x] - array[index][x], 2)
    + (Math.pow((array[puntoPartida][y] - array[index][y]), 2))));
  }
  return arrayDistances;
}

/* Función que comprueba si estamos ante un rectángulo ya que mediante la siguiente
fórmula comprueba que la diagonal vale igual que raíz de la suma de la base más la altura al cuadrado (Teorema de Pitágoras) */
function comprobarRectangulo(array, diagonal) {
  return Math.sqrt(Math.pow(array[0], 2) + Math.pow(array[1], 2)) === diagonal;
}

/* Función que calcula el área multiplicando la base y la altura.
Para ello deja la diagonal de lado, ya que está es más grande que la base y la altura*/
function calculateArea(array) {
  let greaterDistance = Math.max.apply(null, array); // Truco para obtener el máximo de un vector
  array.splice(array.indexOf(greaterDistance, 1)); // Eliminamos el mayor
  if (comprobarRectangulo(array, greaterDistance)) {
    return array.reduce((a, b) => a * b) + " es el área";
  } else {
    return "los cuatro puntos no corresponden a un rectángulo";
  }
}

/* Función que se encarga de llamar a los diferentes métodos que se 
encargan que es un rectángulo */
function areaRectangulo(array) {
  if (array.length === 4) {
    return calculateArea(calculateDistance(parsePoints(array)));
  } else {
    return "necesitamos 4 puntos para formar un rectángulo, así que debe introducir los puntos de forma correcta."
  }
}

/*
  const puntos = ["(1 1)","(1 3)","(3 1)","(3 3)"];
  const puntos = ["(-2 6)","(-2 3)","(4 6)","(4 3)","(4 3)"];
  const puntos = ["(-2 6)","(-2 3)","(4 6)","(4 3)"];
  const puntos = [" ( -2  6 ) ","(-2 3)","(4 6)","(4 3)"];
*/
const puntos = ["(1 1)","(1 3)","(3 1)","(3 3)"];
console.log(`Con los puntos dados ${puntos} obtenemos que ${areaRectangulo(puntos)}`);