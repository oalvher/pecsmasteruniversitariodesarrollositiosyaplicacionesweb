// Función que devuelve el valor según el valor del dinero que nos den
function valorSegunString(valor) {
  switch (valor) {
    case "50EUROS":
      return 50;
    case "20EUROS":
      return 20;
    case "10EUROS":
      return 10;
    case "5EUROS":
      return 5;
    case "1EURO":
      return 1;
    case "25CENTIMOS":
      return 0.25;
    case "25CENTIMOS":
      return 0.25;
    case "10CENTIMOS":
      return 0.10;
    case "1CENTIMO":
      return 0.01;
  }
}

// Comprobamos que estado debemos devolver
function comprobarEstadoEfectivo(efectivo, removeElementArray) {
  /* Si la caja está vacía debemos cerrarla ya que el efectivo restante es cero o si hemos descartado algún elemento del vector significa que hay dinero de algún otro
  billete o moneda */
  if (efectivo.filter(element => element[1] > 0).length === 0 && !removeElementArray) {
    return "CERRAR";
  } else { // En caso contrario, seguirá abierta.
    return "ABRIR";
  }
}

function devolverDinero(cambioTotal, efectivo) {
  // Establecemos un vector según los posibles valores que forman parte del efectivo
  let solucion = []; 
  let removeElementArray = false;
  while (efectivo.length !== 0) {
    let cambioTotalCopia = cambioTotal;
    let efectivoCopia = efectivo;
    for (let index = 0; index < efectivo.length; index++) { // Recorremos cada valor del cambio para ver cuanto debemos empezar a devolver
      let valorDinero = valorSegunString(efectivo[index][0]);
      let cambioDinero = 0;
      if (valorDinero <= cambioTotal) {
        while (valorDinero <= cambioTotal && efectivo[index][1] !== 0) { // Mientras podamos devolver dinero según el valor, seguimos sumando en el cambio
          cambioTotal = parseFloat((cambioTotal - valorDinero).toFixed(2)); // Restamos dinero al cambio total
          cambioDinero = parseFloat((cambioDinero + valorDinero).toFixed(2)); // Sumamos dinero al cambio que vamos a devolver
          efectivo[index][1] = parseFloat((efectivo[index][1] - valorDinero).toFixed(2)); // Restamos dinero al efectivo de la caja
        }
        solucion.push([efectivo[index][0], cambioDinero]);
      }
    }
    if (cambioTotal === 0) { // Si el cambio es cero, tenemos solucion
      return {estado: comprobarEstadoEfectivo(efectivo, removeElementArray), cambio: solucion};
    } else { // Si el cambio no está saldado, seguimos probando con el vector reducido.
      cambioTotal = cambioTotalCopia;
      removeElementArray = true;
      efectivoCopia.splice(0, 1);
      efectivo = efectivoCopia;
      solucion = [];
    }
  }
  return {estado: "FONDOS_INSUFICIENTES", cambio: []};
}

// Función que comprueba cuánto dinero debe devolver
function cajaRegistradora(compra, pago, efectivo) {
  if (pago < compra) { // Si falta dinero se muestra
    return {estado: "FALTA_DINERO", cambio: []}
  } else if (pago === compra) { // Si ha pagado de forma exacta se muestra
    return {estado: "QUE TENGA BUEN DÍA!!!", cambio: []}
  } else { // Si debemos devolver dinero, se debe dar cambio.
    // Pasamos el vector aquellos elementos válidos, es decir, mayores que 0
    return devolverDinero(pago - compra, efectivo.filter(element => element[1] > 0).reverse()); 
  }
}

// Comprueba si el valor es correcto con respecto al valor del dinero
function checkMoneyCorrect(money) {
  if (money[1] % 1 === 0) { // Comprobar si es entero
    return money[1] % valorSegunString(money[0]) !== 0;
  } else {
    let value = money[1];
    while(value > 0) {
      value = parseFloat((value - valorSegunString(money[0])).toFixed(2));
    }
    return value !== 0;
  }
}

/* let efectivoExistente = [["1CENTIMO", 0], ["10CENTIMOS", 0], ["25CENTIMOS", 0], ["1EURO", 0], ["5EUROS", 5], ["10EUROS", 0], ["20EUROS", 60], ["50EUROS", 50]];
let compra = 50;
let pago = 100; 

let compra = 50.5;
let pago = 60;
let efectivoExistente = [["1CENTIMO", 1.01],["10CENTIMOS", 3.10],["25CENTIMOS", 4.25],["1EURO", 20],["5EUROS", 25],["10EUROS", 60],["20EUROS", 60],["50EUROS", 100]];
*/
let compra = 50.5;
let pago = 60;
let efectivoExistente = [["1CENTIMO", 1.01],["10CENTIMOS", 3.10],["25CENTIMOS", 4.25],["1EURO", 20],["5EUROS", 25],["10EUROS", 60],["20EUROS", 60],["50EUROS", 100]];
if (efectivoExistente.filter(element => typeof element[0] !== "string" || typeof element[1] !== "number").length !== 0) { 
  console.log("LOS DATOS DEL EFECTIVO DEBEN SER: [NÚMERO, STRING]. COMPRUEBE EL EFECTIVO INTRODUCIDO");
} else if (efectivoExistente.filter(element => checkMoneyCorrect(element)).length !== 0) {
  console.log("EN LA CAJA EXISTEN CANTIDADES ERRÓNEAS CON RESPECTO AL VALOR");
} else if (typeof compra !== "number" || typeof pago !== "number") {
  console.log("LA COMPRA Y EL PAGO DEBEN SER NÚMEROS");
} else {
  console.log(cajaRegistradora(compra, pago, efectivoExistente));
}

