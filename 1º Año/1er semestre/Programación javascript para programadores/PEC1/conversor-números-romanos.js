// Principales números romanos
const possibleValues= [["M", 1000], ["CM", 900], ["D", 500], ["CD", 400], ["C", 100], ["XC", 90], ["L", 50], ["XL", 40], ["X", 10], ["IX", 9], ["V", 5], ["IV", 4], ["I", 1]]; 
const indexKey = 0;
const indexValue = 1;

// Función que convierte un número arábigo en romano
function romanNumeralConverter(number) {
  if (number === 1) { // Acaban aquí aquellos números no contemplados en el vector
    return "I";
  } else if (number === 0) { // Acaban aquí aquellos números contemplados en el vector
    return "";
  } else {
    for (let index = 0; index < possibleValues.length; index++) {
      if (possibleValues[index][indexValue] <= number) { // Si el valor es menor o igual que el número en concreto, es el elegido para formar parte del número romano
        // Invocamos de nuevo a la misma función pero con el número ya restado, y habiendo puesto el correcto carácter romano.
        return possibleValues[index][indexKey] + romanNumeralConverter(number - possibleValues[index][indexValue]); 
      } 
    }
  }
}
/*
const number = 1986;
const number = 96;
const number = 204;
const number = 1995;
const number = -1;
const number = 1840;
const number = "1840";
*/
const number = 2018;
if (typeof(number) !== "number") {
  console.log("Debe introducir un número");
} else if (number <= 0) {
  console.log("El número debe ser mayor que 0");
} else {
  console.log(`El resultado para ${number} es ${romanNumeralConverter(number)}`);
}
