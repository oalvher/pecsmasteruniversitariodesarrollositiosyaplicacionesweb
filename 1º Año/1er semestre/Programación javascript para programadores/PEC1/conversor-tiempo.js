/* Función que si el número de segundos es menor que 10 le añade un 0 */
function normalize(minutes) {
  return minutes < 10 ? "0" + minutes : minutes;
}

/* Función que convierte el número a XX:YY */
function timeConverter(number) {
    if (typeof number != "number") { // Se comprueba si es un número
      return "Debe introducir un número";
    } if (number < 0) { // Si el número es menor que 0 no se puede realizar la conversión
      return "El número de minutos debe ser mayor que 0";
    } else {
      /* Divide el número entre 60 para obtener el número de horas exacto y redondea por debajo, y por otro 
      lado calcula el resto para obtener el número de minutos */
      return `${Math.floor(number / 60)}:${normalize(number % 60)}`; 
    }
}

/*
const number = 10;
const number = 9;
const number = 217;
const number = "217";
const number = -217;
const number = 126;
*/
const number = 66;
console.log(timeConverter(number));
