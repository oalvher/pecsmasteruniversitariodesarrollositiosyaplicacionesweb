/* Función que se encarga de eliminar todos aquellos caracteres que no sean ni números ni letras, además de sustituir
aquellos caracteres que tienen tildes, por el mismo carácter sin la tilde. */
function parseString(string) {
  return string.toLowerCase()
    .replace(/á/g, "a")
    .replace(/é/g, "e")
    .replace(/í/g, "i")
    .replace(/ó/g, "o")
    .replace(/ú/g, "u")
    .replace(/[^a-zA-Z0-9]/g, "");
}

/* Función que recibe la cadena parseada, y recorre la misma con dos índices, uno que empieza al inicio de la cadena y otro
que empieza al final, de tal forma que se vaya comparando que los caracteres sean iguales. Dicha comparación se realiza mientras el 
índice de inicio sea menor que el de final, ya que de esta forma hemos analizado las dos mitades de la cadena, y en caso de ser impar, el
caracter del medio no nos aporta nada para la detencción de cadenas palíndromas. */
function detectPalindrome(string) {
  let start = 0;
  let end = string.length - 1;
  while (start < end) {
    if(string[start] !== string[end]) {
      return false;
    }
    start++;
    end--;
  }
  return true;
}

/*
const string = "Somos o no somos";
const string = "¿Acaso hubo búhos acá?";
const string = "Lamina animal";
const string = "La ruta natural";
const string = "Abajo me mojaba";
const string = "Oír a Darío";
const string = "La era real";
*/
const string = "Dábale arroz a la zorra el abad";
if (string.length === 0 || typeof string !== "string") {
  console.log("Introduzca una cadena");
} else  {
  console.log(`La cadena \"${string}\" ${detectPalindrome(parseString(string)) ? "es palíndroma" : "no es palíndroma"}`);
}
