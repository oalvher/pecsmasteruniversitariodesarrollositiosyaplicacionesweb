/* Clase superheroe con su nombre y descripción */
export class SuperHero {

  constructor(name, description) {
    this.name = name;
    this.description = description;
  }

}