export const API_URL = "https://gateway.marvel.com:443/v1/public"; // Url de la api de marvel
export const ENDPOINT_CHARACTERES = "/characters"; // El endpoint de donde se obtiene los superheroes
export const ENDPOINT_COMICS = "/comics"; // El endpoint de donde se obtiene los cómics
export const RESPONSE_OK = 200; // Respuesta OK, es decir, obtenemos datos de la consulta
export const MINIMUM_NUMBER_CHARACTERS_COMIC = 3; // Número mínimo de cómics para poder continuar con el objetivo de la práctica.
export const CHARACTERES = ["Spider-Man", "Batman", "Captain America", "Superman", "Hulk", "Iron Man"]; // Vector de donde se obtiene el personaje principal