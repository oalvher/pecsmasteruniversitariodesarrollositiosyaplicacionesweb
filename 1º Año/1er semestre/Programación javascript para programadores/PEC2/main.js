import { getCharacterId, getComicsByCharacter, getDataSecondaryCharacter } from "./marvel-service.js";
import { CHARACTERES, MINIMUM_NUMBER_CHARACTERS_COMIC } from "./constants.js";
import { SuperHero } from "./classes.js";

// Devuelve el objeto superheroe
function getCharacter(urlCharacter, nameCharacter) {
    return getDataSecondaryCharacter(urlCharacter).then(character => {
        let characterObject = new SuperHero(nameCharacter, character.description);
        return characterObject;
    });
}

function getComics(nameSuperhero, idSuperhero) {
    console.log("Obteniendo comics del personaje... Espere por favor, se están obteniendo 100 cómics");
    return getComicsByCharacter(idSuperhero)
    .then(comics => {
        // Obtenemos los comics con más de un personaje
        let comicsWithMoreOneCharacter = comics.filter(result => result.characters.items.length > 1);
        // Comprobamos si hay mínimo 3 para poder continuar y sino poder mostrar un mensaje de error
        if (comicsWithMoreOneCharacter.length >= MINIMUM_NUMBER_CHARACTERS_COMIC) {
            // Obtenemos al menos 3 cómics
            comicsWithMoreOneCharacter = comicsWithMoreOneCharacter.slice(0, MINIMUM_NUMBER_CHARACTERS_COMIC);
            // Recorremos cada cómic para luego recorrer cada personaje e ir generando promesas
            comicsWithMoreOneCharacter.map(comic => {
                /* Descartamos al personaje principal para que sólo aparezcan el resto. Se hace de esta forma para no estar
                repitiendo en todo momento la descripción */
                let characteres = comic.characters.items.filter(character => character.name !== nameSuperhero);
                let promises = characteres.map(character => { 
                    return getCharacter(character.resourceURI, character.name);
                });
                // Se muestran los resultados cuando todos los personajes tengan toda la información necesaria.
                Promise.all(promises).then(characters => {
                    console.log('Título comic: ', comic.title);
                    for (let character of characters) {
                        console.log("\t Nombre personaje: ", character.name);
                        console.log("\t Descripción personaje: ", character.description.length === 0 ? "No tiene descripción" : 
                        character.description);
                    }
                });
            });
        } else {
            throw "No hay mínimo 3 comics con al menos un personaje distinto al superhéroe principal";
        }
    })
    .catch(err => console.log(err));
}

// Obtener un superheroe del vector para poder hacer en cada llamada un caso diferente
let nameSuperhero = CHARACTERES[Math.round(Math.random() * (CHARACTERES.length - 1))];
console.log("Obteniendo id del personaje " + nameSuperhero + "...");
// Si hay resultados seguimos con la ejecución y en caso contrario mostramos un mensaje de error.
getCharacterId(nameSuperhero)
    .then(data => {
        // Si el personaje existe, lo devolvemos
        if (data.results.length > 0) {
            return data.results[0];
        } else {
            throw nameSuperhero + " no es un personaje de Marvel";
        } 
    })
    .then(character => {
        /* Mostramos al menos una vez la descripción del personaje principal */
        console.log("Descripción personaje principal: " + character.description);
        getComics(character.name, character.id);
    })
    .catch(err => console.log(err));