import { getCharacterId, getComicsByCharacter, getDataSecondaryCharacter } from "./marvel-service.js";
import { MINIMUM_NUMBER_CHARACTERS_COMIC } from "./constants.js";
import { SuperHero } from "./classes.js";

// Devuelve el objeto superheroe
function getCharacter(urlCharacter, nameCharacter) {
    return getDataSecondaryCharacter(urlCharacter).then(character => {
        const characterObject = new SuperHero(nameCharacter, character.thumbnail.path + "." + character.thumbnail.extension);
        return characterObject;
    });
}

function getComics(nameSuperhero, idSuperhero) {
    waiting("#loading-comics", ".comicList");
    return getComicsByCharacter(idSuperhero)
    .then(comics => {
        const comicsHtml = document.querySelectorAll(".comicListItem");
        let index = 0;
        // Obtenemos los comics con más de un personaje
        let comicsWithMoreOneCharacter = comics.filter(result => result.characters.items.length > 1);
        // Comprobamos si hay mínimo 3 para poder continuar y sino poder mostrar un mensaje de error
        if (comicsWithMoreOneCharacter.length >= MINIMUM_NUMBER_CHARACTERS_COMIC) {
            // Obtenemos al menos 3 cómics
            comicsWithMoreOneCharacter = comicsWithMoreOneCharacter.slice(0, MINIMUM_NUMBER_CHARACTERS_COMIC);
            // Recorremos cada cómic para luego recorrer cada personaje e ir generando promesas
            comicsWithMoreOneCharacter.map(comic => {
                /* Descartamos al personaje principal para que sólo aparezcan el resto. Se hace de esta forma para no estar
                repitiendo en todo momento la descripción */
                const characteres = comic.characters.items.filter(character => character.name !== nameSuperhero);
                const promises = characteres.map(character => { 
                    return getCharacter(character.resourceURI, character.name);
                });
                // Se muestran los resultados cuando todos los personajes tengan toda la información necesaria.
                Promise.all(promises).then(characters => {
                    while (comicsHtml[index].children.item(2).hasChildNodes()) {
                        comicsHtml[index].children.item(2).removeChild(comicsHtml[index].children.item(2).lastChild);
                    }
                    comicsHtml[index].children.item(0).innerHTML = comic.title;
                    comicsHtml[index].children.item(4).innerHTML = comic.description ? comic.description : "No description";
                    /* Para cada superheroe creamos su li donde ira la imagen del mismo */
                    for (let character of characters) {
                        const li = document.createElement("LI");
                        const img = document.createElement("IMG");
                        img.src = character.imgUrl;
                        img.title = character.name;
                        img.alt = character.name;
                        img.className += " " + "portrait";
                        li.appendChild(img);
                        comicsHtml[index].children.item(2).appendChild(li);
                    }
                    index++;
                    /* Ya ha finalizado el proceso de obtención de cómics */
                    if (index > 2) {
                        showResult('#loading-comics', ".comicList");
                    }
                });
            });
        } else {
            throw "No hay mínimo 3 comics con al menos un personaje distinto al superhéroe principal";
        }
    })
    .catch(err => {
        showError(err);
    });
}

export function getMainCharacter(nameSuperhero) {
    removeWelcomeAndError();
    waiting('.loading', ".heroInfo");
    hideComics();
    getCharacterId(nameSuperhero)
    .then(data => {
        // Si el personaje existe, lo devolvemos
        if (data.results.length > 0) {
            return data.results[0];
        } else {
            throw nameSuperhero + " no es un personaje de Marvel";
        } 
    })
    .then(character => {
        document.querySelector("#heroDescription h1").innerHTML = character.name;
        document.querySelector("#heroDescription p").innerHTML = character.description ? character.description : "No description";
        document.querySelector(".heroInfo img").src = character.thumbnail.path + "." + character.thumbnail.extension;
        showResult('.loading', ".heroInfo");
        getComics(character.name, character.id);
    })
    .catch(err => {
        showError(err);
    });
}

/* Muestra un load-spinner para mostrar que se está esperando por la respuesta */
function waiting(loading, element) {
    document.querySelector(loading).style.display = "block";
    document.querySelector(element).style.display = "none";
}

/* Muestra los resultados */
function showResult(loading, element) {
    document.querySelector(loading).style.display = "none";
    document.querySelector(element).style.display = "flex";
}

/* Hace desaparecer el mensaje inicial de bienvenida */
function removeWelcomeAndError() {
    document.querySelector('#welcome').style.display = "none";
    document.querySelector('#error').style.display = "none";
    document.querySelector('#result').style.display = "block";
}

/* Muestra errores por pantalla */
function showError(err) {
    document.querySelector('#result').style.display = "none";
    document.querySelector('#error').style.display = "block";
    document.querySelector('#error span').innerHTML = err;
}

/* Oculta el apartado de comics */
function hideComics() {
    document.querySelector(".comicList").style.display = "none";
}