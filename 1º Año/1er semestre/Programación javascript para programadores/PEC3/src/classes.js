/* Clase superheroe con su nombre y descripción */
export class SuperHero {

  constructor(name, imgUrl) {
    this.name = name;
    this.imgUrl = imgUrl;
  }

}