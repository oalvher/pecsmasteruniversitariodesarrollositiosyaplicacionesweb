export const PUBLIC_KEY = "10401530fd0cd3cab69df2d40fb58539"; // Clave pública necesaria para la construcción de la query
export const TIMESTAMP = 1; // Timestamp necesario para la construcción de la query
export const HASH = "46915a32e67b09ffa3f41edb28b7da28"; // Hash que se genera de forma online para la query
export const QUERYPARAM = `?apikey=${PUBLIC_KEY}&ts=${TIMESTAMP}&hash=${HASH}`; // Query que permite la autorización contra la api