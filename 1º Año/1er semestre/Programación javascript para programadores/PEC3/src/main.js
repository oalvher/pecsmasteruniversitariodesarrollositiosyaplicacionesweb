import { getMainCharacter } from "./api.js";

/* Evento que llama al flujo principal cuando clickemos el botón */
const myButton = document.querySelector("#button");
myButton.addEventListener("click", function() {
    const superHero = document.querySelector("#input").value;
    if (superHero) {
        getMainCharacter(superHero);
    }
});

/* Evento que llama al flujo principal cuando pulsemos enter sobre el input del formulario */
const myInput = document.querySelector("#input");
myInput.addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        const superHero = document.querySelector("#input").value;
        if (superHero) {
            getMainCharacter(superHero);
        }
    }
});

/* Técnica de delegación de eventos para poder activar la búsqueda pulsando sobre una imagen */
const imgSuperHero = document.querySelector('.comicList');
imgSuperHero.addEventListener('click', e => {
    if (e.target.classList.contains('portrait')) {
        getMainCharacter(e.target.alt);
    }
});