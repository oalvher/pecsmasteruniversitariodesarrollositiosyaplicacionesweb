import { API_URL, ENDPOINT_CHARACTERES, ENDPOINT_COMICS, RESPONSE_OK, MESSAGE_ERROR_HTTP } from "./constants.js";
import { QUERYPARAM } from "./configuration.js";

/* Llamada a la api para obtener la información de un personaje. Se devuelve el campo data del json. En caso de que se produca
un error en la llamada, se muestra un mensaje de error */
export function getCharacterId(nameCharacter) {
    const url = API_URL + ENDPOINT_CHARACTERES + QUERYPARAM + "&name=" + nameCharacter;
    return fetch(url)
        .then(response => {
            if (response.status !== RESPONSE_OK) {
                throw MESSAGE_ERROR_HTTP + response.status;
            }
            return response.json();
        })
        .then(json => json.data)
        .catch(function(err) {
            console.log(err);
        });
}

/* Llamada a la api para obtener los cómics del superheroe. Se devuelve el campo data del json. En caso de que se produca
un error en la llamada, se muestra un mensaje de error */
export function getComicsByCharacter(idSuperhero) {
    const url = API_URL + ENDPOINT_CHARACTERES + "/" + idSuperhero + ENDPOINT_COMICS + QUERYPARAM + "&limit=" + 100;
    return fetch(url)
        .then(response => {
            if (response.status !== RESPONSE_OK) {
                throw MESSAGE_ERROR_HTTP + response.status;
            }
            return response.json();
        })
        .then(json => json.data.results)
        .catch(function(err) {
            console.log(err);
        });
}

/* Llamada a la api para consultar directamente un endpoint. Se devuelve el campo data del json. 
En caso de que se produca un error en la llamada, se muestra un mensaje de error */
export function getDataSecondaryCharacter(url) {
    return fetch(url + QUERYPARAM)
        .then(response => {
            if (response.status !== RESPONSE_OK) {
                throw MESSAGE_ERROR_HTTP + response.status;
            }
            return response.json();
        })
        .then(json => json.data.results[0])
        .catch(function(err) {
            console.log(err);
        });
}