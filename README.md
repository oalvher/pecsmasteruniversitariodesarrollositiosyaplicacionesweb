Máster Universitario en Desarrollo de Sitios y Aplicaciones Web

Programación
- Herramientas de prototipado
    - Justinmind
    - Axure
    - InVision
    - Balsamiq
- Lenguaje de programación de desarrollo web: HTML
- Lenguaje de programación de diseño web: CSS
    - Guías de estilo
    - Metodologías CSS: OOCSS, SMACSS, BEM, ...
    - Grid y Flex
- Lint css : Stylelint
- Diseños, colores, tipografías,...: Material Design (https://material.io/)
- Lenguaje de programación web: Javascript
- Frameworks visuales o librerías de componentes: Bootstrap
- Librerías de utilidades: TailwindCss (Atomic CSS)
- Preprocesadores javascript: Typescript y Babel
- Preprocesadores css: PostCSS más el plugin Autoprefixer y Sass. Otro es Less
- Gestores de contenidos (CMS): Drupal
- Frameworks php: CodeIgniter y Laravel
- Frameworks javascript: Angular
    - Principal: Componentes, servicios, router, directivas, pipes, ... 
    - Formularios reactivos
    - Ngrx: Redux + Rxjs
    - Angular Material
    - Pruebas: Jasmine/Karma
- Frameworks para aplicaciones híbridas: Ionic

Herramientas de programación
- Servidores web: XAMPP
- Uso de terminal
- Editores: Sublime Text y Atom
- IDEs: Visual Studio Code
- Control de versiones: Git, Gitlab (Gestor de repositorios) y Sourcetree (IDE Git)
- Node y NVM
- Gestores de paquetes: npm. Otro es Yarn
- Build system o task runners: Gulp más Browsersync 
- Module bundler: Parcel. Otro es Webpack
- Publicar en internet a través de: Github para repositorio y Netlify como proveedor para publicar en Internet